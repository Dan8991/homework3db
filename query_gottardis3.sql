-- connect to vims db
\c vims

-- Query to retrieve the city with the maximum number of clients
SELECT Location, NumClient
	FROM (SELECT Location, COUNT(TaxCode) AS NumClient
	FROM Client 
	GROUP BY Location) AS Count
WHERE NumClient=(SELECT MAX(NumClient)
				 FROM (SELECT Location, COUNT(TaxCode) AS NumClient
				 	   FROM Client
				 	   GROUP BY Location) AS Count2);
