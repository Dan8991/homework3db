-- connect to vims db
\c vims

-- Query to retrieve Position, Variety Name and Size of all the up to date vineyards of a client
SELECT Province, Location, Name, VineyardSize
	FROM Vineyard AS Vi INNER JOIN Variety AS Va
		ON Vi.VarietyCode = Va.Code
	WHERE Vi.UpToDate = TRUE AND Vi.ClientId = 'FNTCDC00A01A123C';

-- Query to retrieve all the clients of an Intermediary with their personal informations
SELECT CType AS TypeOfCustomer, Surname, Name, Address, Location AS City, Province, Telephone, Email
	FROM Client
	WHERE IntermediaryId ='F435466856'
	ORDER BY TypeOfCustomer ASC, Client.Surname ASC, Client.Name ASC;

-- Query to retrieve all the claims with their info made by all the clients 
SELECT Ctype AS TypeOfCustomer, Surname, Name, TaxCode, Claim.ClaimID, State, ClaimDate
	FROM Claim INNER JOIN ReferTo AS R ON Claim.ClaimID = R.ClaimID
		INNER JOIN Vineyard AS V ON R.VineyardId = V.VineyardId
		INNER JOIN Client ON V.ClientID = Client.TaxCode
	ORDER BY TypeOfCustomer ASC, Client.Surname ASC, Client.Name ASC;

-- Query to retrieve the number of active policies sold (Sold this year) by each Intermediary
SELECT Surname, Name, SoldPolicies
	FROM (SELECT Surname, Name, COUNT(PolicyId) AS SoldPolicies
		FROM InsurancePolicy AS IP INNER JOIN Intermediary AS I ON IP.IntermediaryId = I.RuiId
		WHERE NOW() < UPPER(PolicyValidity)
		GROUP BY Surname, Name) AS Count
	ORDER BY SoldPolicies DESC;

-- Query to retrieve the number of active policies sold (Sold this year) for each Company
SELECT Name, SoldPolicies
	FROM (SELECT Name, COUNT(PolicyId) AS SoldPolicies
		FROM InsurancePolicy AS Po INNER JOIN InsurancePackage AS Pa ON Po.PackageId = Pa.PackageId
		INNER JOIN InsuranceCompany AS Co ON Pa.VatId = Co.VatId
		WHERE NOW() < UPPER(PolicyValidity)
		GROUP BY Name) AS Count
	ORDER BY SoldPolicies DESC;

-- Query to retrieve the list of variety ordered based on their popularity (how many up to date vineyards with that variety has been insured)
SELECT Name, Code, Yield, Price, NumberOfVineyards
	FROM (SELECT Name, Code, Yield, Price, COUNT(VineyardId) AS NumberOfVineyards
		FROM Variety AS Va INNER JOIN Vineyard AS Vi ON Va.Code = Vi.VarietyCode
		WHERE Vi.UpToDate = TRUE
		GROUP BY Code) AS Count
	ORDER BY NumberOfVineyards DESC;