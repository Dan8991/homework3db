-- connect to vims db
\c vims

-- Query to retrive the total amount of policies sold by a intermediary grouped by year
SELECT COUNT(policyid) as totalsold,
    EXTRACT(year FROM LOWER(policyvalidity)) as policyyear
FROM InsurancePolicy
WHERE intermediaryid ='F152578620'
GROUP BY policyyear
ORDER BY policyyear DESC;

-- Query to retrive the total amount of policies sold by a intermediary grouped by Guarantee 
SELECT COUNT(IP.policyid) as total, 
    G.code, 
    G.name
FROM InsurancePolicy AS IP 
INNER JOIN Define AS D
    ON IP.packageid = D.packageid
INNER JOIN Guarantee AS G
    ON D.GuaranteeCode = G.code
WHERE intermediaryid ='F152578620'
GROUP BY G.code, G.name;

-- Query to retrive an available Insurance Package with the same guarantees of a given package (packageid = 3 in this case)
WITH GuaranteeInfo AS(
    SELECT packageid, string_agg(GuaranteeCode, ' ' ORDER BY GuaranteeCode) AS GuaranteeString
    FROM Define
    GROUP BY packageid) 
SELECT Old.packageid AS oldpackage,
    New.packageid AS newpackage,
    New.GuaranteeString AS guarantees, 
    C.name AS newcompany
FROM GuaranteeInfo AS Old 
INNER JOIN GuaranteeInfo AS New
    ON New.GuaranteeString = Old.GuaranteeString
INNER JOIN InsurancePackage AS P
    ON New.packageid = P.packageid
INNER JOIN InsuranceCompany AS C
    ON P.vatid = C.vatid
WHERE New.packageid != Old.packageid
    AND Old.packageid = 3
    AND P.available;

--Same Query without using WITH
SELECT Old.packageid AS oldpackage,
    New.packageid AS newpackage,
    New.GuaranteeString AS guarantees, 
    C.name AS newcompany
FROM (
    SELECT packageid, string_agg(GuaranteeCode, ' ' ORDER BY GuaranteeCode) AS GuaranteeString
    FROM Define
    GROUP BY packageid) AS Old 
INNER JOIN (
    SELECT packageid, string_agg(GuaranteeCode, ' ' ORDER BY GuaranteeCode) AS GuaranteeString
    FROM Define
    GROUP BY packageid) AS New
    ON New.GuaranteeString = Old.GuaranteeString
INNER JOIN InsurancePackage AS P
    ON New.packageid = P.packageid
INNER JOIN InsuranceCompany AS C
    ON P.vatid = C.vatid
WHERE New.packageid != Old.packageid
    AND Old.packageid = 3
    AND P.available;

--Query to retrive the total amount of money earned by the intermediaries this year in decresing order
SELECT I.RuiId,
	I.name,
	I.surname,
	SUM(PO.InsuredCapital * PO.PercentageIntermediary * PA.rate) AS earned
FROM Intermediary AS I
INNER JOIN InsurancePolicy AS PO
	ON PO.IntermediaryId = I.RuiId
INNER JOIN InsurancePackage AS PA
	ON PO.packageid = PA.packageid
WHERE EXTRACT(year FROM LOWER(policyvalidity)) = EXTRACT(year FROM current_date)
GROUP BY I.RuiId
ORDER BY earned DESC;

-- Query to retrieve Position, Variety Name and Size of all the up to date vineyards of a client
SELECT Province, Location AS City, Name AS VarietyName, VineyardSize
	FROM Vineyard AS Vi 
	INNER JOIN Variety AS Va
		ON Vi.VarietyCode = Va.Code
	WHERE Vi.UpToDate = TRUE
		AND Vi.ClientId = 'FNTCDC00A01A123C';

-- Query to retrieve all the clients of an Intermediary with their personal informations
SELECT CType AS TypeOfCustomer, Client.Surname, Client.Name, Address, Location AS City, Province, Telephone, Email
	FROM Client
	WHERE IntermediaryId ='F435466856'
	ORDER BY TypeOfCustomer ASC, Client.Surname ASC, Client.Name ASC;

-- Query to retrieve all the claims with their info made by all the clients 
SELECT DISTINCT Ctype AS TypeOfCustomer, Client.Surname, Client.Name, TaxCode, Claim.ClaimID, State, ClaimDate
	FROM Claim 
	INNER JOIN ReferTo AS R 
		ON Claim.ClaimID = R.ClaimID
	INNER JOIN Vineyard AS V 
		ON R.VineyardId = V.VineyardId
	INNER JOIN Client 
		ON V.ClientID = Client.TaxCode
	ORDER BY TypeOfCustomer ASC, Client.Surname ASC, Client.Name ASC;

-- Query to retrieve the number of active policies sold by each Intermediary
SELECT Surname, Name, COUNT(PolicyId) AS SoldPolicies
		FROM InsurancePolicy AS IP INNER JOIN Intermediary AS I ON IP.IntermediaryId = I.RuiId
		WHERE NOW() < UPPER(PolicyValidity)
		GROUP BY Surname, Name
	ORDER BY SoldPolicies DESC;

-- Query to retrieve the number of active policies sold for each Company
SELECT Name, COUNT(PolicyId) AS SoldPolicies
	FROM InsurancePolicy AS Po 
	INNER JOIN InsurancePackage AS Pa 
		ON Po.PackageId = Pa.PackageId
	INNER JOIN InsuranceCompany AS Co 
		ON Pa.VatId = Co.VatId
	WHERE NOW() < UPPER(PolicyValidity)
	GROUP BY Name
	ORDER BY SoldPolicies DESC;

-- Query to retrieve the list of variety ordered based on their popularity (how many up to date vineyards with that variety has been insured)
SELECT Name, Code, Yield, Price, COUNT(VineyardId) AS NumberOfVineyards
	FROM Variety AS Va INNER JOIN Vineyard AS Vi ON Va.Code = Vi.VarietyCode
	WHERE Vi.UpToDate = TRUE
	GROUP BY Code
	ORDER BY NumberOfVineyards DESC;

-- Query to retrieve the name of the most expensive variety
SELECT Name
	FROM Variety 
	WHERE Price = ( SELECT MAX(Price) FROM Variety );

-- Query to retrieve the city with the maximum number of clients
SELECT Location, NumClient
	FROM (SELECT Location, COUNT(TaxCode) AS NumClient
	FROM Client
	GROUP BY Location) AS Conteggio
WHERE NumClient=(SELECT MAX(NumClient)
				 FROM (SELECT Location, COUNT(TaxCode) AS NumClient
				 	   FROM Client
				 	   GROUP BY Location) AS Conteggio2);

-- Query to retrieve the city with the maximum number of clients
SELECT Location, NumClient
	FROM (SELECT Location, COUNT(TaxCode) AS NumClient
	FROM Client 
	GROUP BY Location) AS Count
WHERE NumClient=(SELECT MAX(NumClient)
				 FROM (SELECT Location, COUNT(TaxCode) AS NumClient
				 	   FROM Client
				 	   GROUP BY Location) AS Count2);

-- Query to see the different packets sold in the various provinces
SELECT C.Province, Code, Count(G.Code) AS TypeOccurrences
FROM City AS C INNER JOIN Vineyard AS V 
	ON C.Province=V.Province
INNER JOIN ReferTo AS R
		ON V.VineyardId=R.VineyardId
INNER JOIN Guarantee AS G
		ON R.GuaranteeCode=G.Code
GROUP BY C.Province,Code
ORDER BY C.Province ASC, TypeOccurrences DESC, Code ASC;

-- Query to see the different packets sold in the various regions
SELECT C.Region, Code, Count(G.Code) AS TypeOccurrences
FROM City AS C INNER JOIN Vineyard AS V 
	ON C.Province=V.Province
INNER JOIN ReferTo AS R
		ON V.VineyardId=R.VineyardId
INNER JOIN Guarantee AS G
		ON R.GuaranteeCode=G.Code
GROUP BY C.Region,Code
ORDER BY C.Region ASC, TypeOccurrences DESC, Code ASC;

-- Query to see the province with the maximum claims
SELECT Province, NumClaim
FROM(SELECT CI.Province, COUNT(DISTINCT CL.ClaimID) AS NumClaim
	 FROM Claim AS CL INNER JOIN ReferTo AS R 
	 	ON CL.ClaimID=R.ClaimID
	 INNER JOIN Vineyard AS V
	 	ON R.VineyardID=V.VineyardID
	 INNER JOIN City AS CI
	 	ON V.Province=CI.Province
	 GROUP BY CI.Province) AS Count 
WHERE NumClaim=(SELECT MAX(NumClaim)
	FROM (SELECT CI.Province, COUNT(DISTINCT CL.ClaimID) AS NumClaim
		FROM Claim AS CL INNER JOIN ReferTo AS R 
		ON CL.ClaimID=R.ClaimID
		INNER JOIN Vineyard AS V
		ON R.VineyardID=V.VineyardID
		INNER JOIN City AS CI
		ON V.Province=CI.Province
		GROUP BY CI.Province) AS Count2);