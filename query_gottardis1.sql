-- connect to vims db
\c vims

-- Query to retrieve the name of the most expensive variety
SELECT Name
	FROM Variety 
	WHERE Price = ( SELECT MAX(Price) FROM Variety );
