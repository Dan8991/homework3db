-- connect to vims db
\c vims

-- Query to see the different packets sold in the various provinces
SELECT C.Province, Code, Count(G.Code) AS TypeOccurrences
FROM City AS C INNER JOIN Vineyard AS V 
	ON C.Province=V.Province
INNER JOIN ReferTo AS R
		ON V.VineyardId=R.VineyardId
INNER JOIN Guarantee AS G
		ON R.GuaranteeCode=G.Code
GROUP BY C.Province,Code
ORDER BY C.Province ASC, TypeOccurrences DESC, Code ASC;