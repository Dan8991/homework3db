import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//Lists all the policies in the database

public class PrintPrincipalQueries {


	public static void main(String[] args) {
        final String DRIVER = "org.postgresql.Driver";  //JDBC driver

        //SQL statement used to find packages with the same guarantees as another package
        final String FIND_NEW_PACKAGE = "SELECT Old.packageid AS oldpackage, "
                        +	"New.packageid AS newpackage, "
                        +	"New.GuaranteeString AS guarantees, "
                        +	"C.name AS newcompany "
                        +"FROM ( "
                        +	"SELECT packageid, string_agg(GuaranteeCode, ' ' ORDER BY GuaranteeCode) AS GuaranteeString "
                        +	"FROM Define "
                        +	"GROUP BY packageid) AS Old "
                        +"INNER JOIN ( "
                        +	"SELECT packageid, string_agg(GuaranteeCode, ' ' ORDER BY GuaranteeCode) AS GuaranteeString "
                        +	"FROM Define "
                        +	"GROUP BY packageid) AS New "
                        +	"ON New.GuaranteeString = Old.GuaranteeString "
                        +"INNER JOIN InsurancePackage AS P "
                        +	"ON New.packageid = P.packageid "
                        +"INNER JOIN InsuranceCompany AS C "
                        +	"ON P.vatid = C.vatid "
                        +"WHERE New.packageid != Old.packageid "
                        +	"AND Old.packageid = 3 "
						+	"AND P.available;";
						
		final String MONEY_INTERMEDIARY = "SELECT I.RuiId, "
						+	"I.name, "
						+	"I.surname, "
						+	"SUM(PO.InsuredCapital * PO.PercentageIntermediary * PA.rate) AS earned "
						+"FROM Intermediary AS I "
						+"INNER JOIN InsurancePolicy AS PO "
						+	"ON PO.IntermediaryId = I.RuiId "
						+"INNER JOIN InsurancePackage AS PA "
						+	"ON PO.packageid = PA.packageid "
						+"WHERE EXTRACT(year FROM LOWER(policyvalidity)) = EXTRACT(year FROM current_date) "
						+"GROUP BY I.RuiId "
						+"ORDER BY earned DESC;";

		final String PROVINCE_MAX_CLAIMS = "SELECT Province, NumClaim "
						+"FROM(SELECT CI.Province, COUNT(DISTINCT CL.ClaimID) AS NumClaim "
						+	"FROM Claim AS CL INNER JOIN ReferTo AS R "
						+		"ON CL.ClaimID=R.ClaimID "
						+	"INNER JOIN Vineyard AS V "
						+		"ON R.VineyardID=V.VineyardID "
						+	"INNER JOIN City AS CI "
						+		"ON V.Province=CI.Province "
						+	"GROUP BY CI.Province) AS Count "
						+"WHERE NumClaim=(SELECT MAX(NumClaim) "
						+	"FROM (SELECT CI.Province, COUNT(DISTINCT CL.ClaimID) AS NumClaim "
						+		"FROM Claim AS CL INNER JOIN ReferTo AS R "
						+		"ON CL.ClaimID=R.ClaimID "
						+		"INNER JOIN Vineyard AS V "
						+		"ON R.VineyardID=V.VineyardID "
						+		"INNER JOIN City AS CI "
						+		"ON V.Province=CI.Province "
						+		"GROUP BY CI.Province) AS Count2);";
        
		Connection conn = null;  //connection to DBMS
		Statement stmt = null;  //statement to be executed
		ResultSet rs1 = null;  //result of the statement (1)
		ResultSet rs2 = null;  //result of the statement (2)
		ResultSet rs3 = null;  //result of the statement (3)

		// data structures
		
		//1) data about an old insurance package and a new one with the same guarantees
		String oldpackage = null;
		String newpackage = null;
		String guarantees = null;
		String newcompany = null;

		//2) data about an intermediary and earned money
		String ruiid = null;
		String name = null;
		String surname = null;
		String earned = null;

		//3) data about the province with the maximum claims
		String province = null;
		String numclaim = null;

		try {
			//register the JDBC driver
			Class.forName(DRIVER);
			System.out.println("Driver " + DRIVER +" successfully registered.%n");

		} catch (ClassNotFoundException e) {
			System.out.println("Driver " + DRIVER + " not found: " + e.getMessage());
			System.exit(-1);
		}

		try {

			// connect to the database
			conn = DriverManager.getConnection("jdbc:postgresql://localhost/vims", "postgres", "admin");								

			System.out.println("Correctly Connected to the Database");

			// create the statement to execute the query
			stmt = conn.createStatement();

			System.out.println("Correctly Created Statement");

			// execute query 1
			rs1 = stmt.executeQuery(FIND_NEW_PACKAGE);

			System.out.println("\nQuery 1 successfully executed");
			System.out.println("Query 1 results:");

			//print query results
			while (rs1.next()) {

				oldpackage = rs1.getString("oldpackage");  //old package identifier
				newpackage = rs1.getString("newpackage");  //new package identifier
				guarantees = rs1.getString("guarantees");  //guarantees
				newcompany = rs1.getString("newcompany");  //company that offers the new package
				System.out.println("- " + oldpackage + " " + newpackage + " " + guarantees + " " + newcompany);

			}

			// execute query 2
			rs2 = stmt.executeQuery(MONEY_INTERMEDIARY);

			System.out.println("\nQuery 2 successfully executed");
			System.out.println("Query 2 results:");

			//print query results
			while (rs2.next()) {

				ruiid = rs2.getString("ruiid");  //intermediaty identifier
				name = rs2.getString("name");  //intermediary name
				surname = rs2.getString("surname");  //intermediary surname
				earned = rs2.getString("earned");  //money earned
				System.out.println("- " + ruiid + " " + name + " " + surname + " " + earned);

			}

			// execute query 3
			rs3 = stmt.executeQuery(PROVINCE_MAX_CLAIMS);

			System.out.println("\nQuery 3 successfully executed");
			System.out.println("Query 3 results:");

			//print query results
			while (rs3.next()) {

				province = rs3.getString("province");  //policy identifier
				numclaim = rs3.getString("numclaim");  //insured capital
				System.out.println("- " + province + " " + numclaim);

			}

			
		} catch (SQLException e) {
			System.out.println("Database access error:");

			// cycle in the exception chain
			while (e != null) {
				System.out.println("Message: " + e.getMessage());
				System.out.println("SQL status code: " + e.getSQLState());
				System.out.println("SQL error code: " + e.getErrorCode());
				System.out.println();
				e = e.getNextException();
			}
		} finally {
			try { 
				
				// close the used resources
				if (rs1 != null) {
					rs1.close();
					System.out.println("Result set 1 successfully closed");
				}

				if (rs2 != null) {
					rs2.close();
					System.out.println("Result set 2 successfully closed");
				}

				if (rs3 != null) {
					rs3.close();
					System.out.println("Result set 3 successfully closed");
				}
				
				if (stmt != null) {
					stmt.close();
					System.out.println("Statement successfully closed");
				}
				
				if (conn != null) {
					conn.close();
					System.out.println("Connection successfully closed");
				}
				
				System.out.println("Resources successfully released.");
				
			} catch (SQLException e) {
				System.out.println("Error while releasing resources:");

				//cycling through all exceptions 
				while (e != null) {
					System.out.println("Message: " + e.getMessage());
					System.out.println("SQL status code: " + e.getSQLState());
					System.out.println("SQL error code: " + e.getErrorCode());
					System.out.println();
					e = e.getNextException();
				}

			} finally {
				// release resources to the garbage collector
				rs1 = null;
				rs2 = null;
				rs3 = null;
				stmt = null;
				conn = null;

				System.out.println("Resources released to the garbage collector.");
			}
		}
	}
}
