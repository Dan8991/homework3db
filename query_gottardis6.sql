-- connect to vims db
\c vims

-- Query to see the province with the maximum claims
SELECT Province, NumClaim
FROM(SELECT CI.Province, COUNT(DISTINCT CL.ClaimID) AS NumClaim
	 FROM Claim AS CL INNER JOIN ReferTo AS R 
	 	ON CL.ClaimID=R.ClaimID
	 INNER JOIN Vineyard AS V
	 	ON R.VineyardID=V.VineyardID
	 INNER JOIN City AS CI
	 	ON V.Province=CI.Province
	 GROUP BY CI.Province) AS Count 
WHERE NumClaim=(SELECT MAX(NumClaim)
			    FROM (   SELECT CI.Province, COUNT(DISTINCT CL.ClaimID) AS NumClaim
						 FROM Claim AS CL INNER JOIN ReferTo AS R 
						 	ON CL.ClaimID=R.ClaimID
						 INNER JOIN Vineyard AS V
						 	ON R.VineyardID=V.VineyardID
						 INNER JOIN City AS CI
						 	ON V.Province=CI.Province
						 GROUP BY CI.Province) AS Count2);
