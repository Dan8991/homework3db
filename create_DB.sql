-- database creation
CREATE DATABASE VIMS OWNER POSTGRES ENCODING 'UTF-8';

-- connect to the database to create data for its public schema
\c vims

--downloading extension for sha functions
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

--creating the client type domain
CREATE TYPE ClientType AS ENUM(
    'PRIVATE CLIENT',
    'AGRICOLTURAL ENTERPRISE'
);

--creating the claim status domain
CREATE TYPE ClaimStateType AS ENUM(
    'PENDING',
    'ACCEPTED',
    'REFUSED'
);


--creating a TelephoneNumber type that must be composed by 10 digits
CREATE DOMAIN TelephoneNumber AS VARCHAR(10) 
    CONSTRAINT ProperTel CHECK(value ~ '\d{9,10}');

--creating a VatCode type, this code must be composed by 11 digits
CREATE DOMAIN VatCode AS CHAR(11) NOT NULL CONSTRAINT ProperVat CHECK(value ~ '\d{11}');

--creating a type that can contain either vat code or fiscal code
--A fiscal code must be of the type ccccccddcddcdddc where c are letters and d are digits
--a Vat code must be formed by 11 digits
CREATE DOMAIN VatOrFiscal AS VARCHAR(16) NOT NULL 
    CONSTRAINT ProperVatOrFiscal 
        CHECK(value ~ '^(\d{11}|[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z])$');

--creating a type for the RUI_ID that must have a letter followed by 9 digits
CREATE DOMAIN RuiIdType AS CHAR(10) NOT NULL 
    CONSTRAINT ProperRuiId CHECK(value ~ '[A-F]\d{9}');

--creating a domain for the email that must have 
--making sure that the mail has valid characters before and after the @ and that it has @
CREATE DOMAIN EmailType AS VARCHAR(128) NOT NULL 
    CONSTRAINT ProperEmail CHECK(value ~ '^[A-Za-z0-9.-_]+@[A-Za-z0-9.]+$');

--creating a domain for the variety code
--this code is composed by 5 digits where the first is non zero 
CREATE DOMAIN VarietyCodeType AS CHAR(5) NOT NULL
    CONSTRAINT ProperVarietyCode CHECK(value ~ '\d{5}');

--creating a domain for the province and guarantees
--this code is composed by 2 digits both in capital letters
CREATE DOMAIN TwoLetterCodeType AS CHAR(2) NOT NULL
    CONSTRAINT ProperTwoLetterCode CHECK(value ~ '[A-Z]{2}');

CREATE DOMAIN PercentageType AS DOUBLE PRECISION
    CONSTRAINT ProperPercentage CHECK(value IS NULL OR (value >= 0 AND value <= 1));
    
--Insurance Company
CREATE TABLE InsuranceCompany(
    VatId VatCode,
    Name VARCHAR(128) NOT NULL,
    PRIMARY KEY(VatId)
);

--User
CREATE TABLE VimsUser(
    Email EmailType,
    Password VARCHAR(64) NOT NULL CHECK(Password ~ '[A-Fa-f0-9]{64}'),
    PRIMARY KEY(Email)
);

--Broker 
CREATE TABLE Broker(
    RuiId RuiIdType,
    Name VARCHAR(64) NOT NULL,
    Surname VARCHAR(64) NOT NULL,
    Email EmailType,
    PRIMARY KEY(RuiId),
    FOREIGN KEY ( Email ) REFERENCES VimsUser(Email)
);

--Collaborate
CREATE TABLE Collaborate(
    VatId VatCode NOT NULL,
    RuiId RuiIdType NOT NULL,
    PRIMARY KEY(VatId, RuiId),
    FOREIGN KEY (VatId) REFERENCES InsuranceCompany(VatId),
    FOREIGN KEY (RuiId) REFERENCES Broker(RuiId)
);

--Intermediary
CREATE TABLE Intermediary(
    RuiId RuiIdType,
    Name VARCHAR(64) NOT NULL,
    Surname VARCHAR(64) NOT NULL,
    Email EmailType,
    BrokerId RuiIdType,
    PRIMARY KEY(RuiId),
    FOREIGN KEY (Email) REFERENCES VimsUser(Email),
    FOREIGN KEY (BrokerId) REFERENCES Broker(RuiId)
);

--City, the longest city name in italy has 34 letters so 64 should be enough.
CREATE TABLE City(
    Name VARCHAR(64) NOT NULL,
    Province TwoLetterCodeType,
    Region VARCHAR(32) NOT NULL,
    PRIMARY KEY(Name, Province)
);

--Client
--if the client is private the surname must be not null and the tax code must be the fiscal code 
--else the surname must be null and the tax code must be vat code
--the telephone number should be unique
CREATE TABLE Client(
    TaxCode VatOrFiscal,
    Telephone TelephoneNumber,
    Location varchar(64) NOT NULL,
    Province TwoLetterCodeType,
    Address varchar(128) NOT NULL,
    Name varchar(64) NOT NULL,
    Surname varchar(64),
    CType ClientType NOT NULL,
    IntermediaryId RuiIdType,
    Email EmailType,
    PRIMARY KEY (TaxCode),
    FOREIGN KEY (Email) REFERENCES VimsUser(Email),
    FOREIGN KEY (IntermediaryId) REFERENCES Intermediary(RuiId),
    FOREIGN KEY (Location, Province) REFERENCES City(Name, Province),
    CONSTRAINT SurnameAndIdCheck CHECK(
        (
            CType = 'PRIVATE CLIENT' 
            AND Surname IS NOT NULL 
            AND TaxCode ~ '[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]'
        ) 
        OR
        (
            CType = 'AGRICOLTURAL ENTERPRISE' 
            AND  Surname IS NULL 
            AND  TaxCode ~ '^\d{11}$'
        )
    )
);

--Variety, the yield must be positive
CREATE TABLE Variety(
    Code VarietyCodeType,
    Name VARCHAR(64),
    Yield INTEGER CHECK(Yield > 0),
    Price MONEY,
    PRIMARY KEY(Code)
);

--Vineyard, the size must be positive
CREATE TABLE Vineyard(
    VineyardId SERIAL,
    VineyardSize DOUBLE PRECISION NOT NULL CHECK(VineyardSize > 0),
    UpToDate BOOLEAN NOT NULL,
    VarietyCode VarietyCodeType,
    Province TwoLetterCodeType, 
    Location VARCHAR(64) NOT NULL,
    ClientId VatOrFiscal,
    PRIMARY KEY(VineyardId),
    FOREIGN KEY(Province, Location) REFERENCES City(Province, Name),
    FOREIGN KEY(VarietyCode) REFERENCES Variety(Code),
    FOREIGN KEY(ClientId) REFERENCES Client(TaxCode)
);

--Insurance Package, the deductible and rate must be positive
CREATE TABLE InsurancePackage(
    PackageId SERIAL,
    Available BOOLEAN NOT NULL DEFAULT TRUE,
    Deductible PercentageType NOT NULL,
    Rate PercentageType,
    VatId VatCode,
    PRIMARY KEY(PackageId),
    FOREIGN KEY(VatId) REFERENCES InsuranceCompany(VatId)
);

--Guarantee
CREATE TABLE Guarantee(
    Code TwoLetterCodeType,
    Name VARCHAR(64) NOT NULL,
    PRIMARY KEY(Code)
);

--Define
CREATE TABLE Define(
    PackageId INTEGER NOT NULL,
    GuaranteeCode TwoLetterCodeType,
    PRIMARY KEY(PackageId, GuaranteeCode),
    FOREIGN KEY(PackageId) REFERENCES InsurancePackage(PackageId),
    FOREIGN KEY(GuaranteeCode) REFERENCES Guarantee(Code)
);

--Claim
CREATE TABLE Claim(
    ClaimId SERIAL,
    ClaimDate DATE NOT NULL DEFAULT NOW(),
    State ClaimStateType NOT NULL DEFAULT 'PENDING',
    PRIMARY KEY(ClaimId)
);

--ReferTo
CREATE TABLE ReferTo(
    GuaranteeCode TwoLetterCodeType,
    ClaimId INTEGER NOT NULL,
    VineyardId INTEGER NOT NULL,
    PRIMARY KEY(GuaranteeCode, ClaimId, VineyardId),
    FOREIGN KEY(GuaranteeCode) REFERENCES Guarantee(Code),
    FOREIGN KEY(ClaimId) REFERENCES Claim(ClaimId),
    FOREIGN KEY(VineyardId) REFERENCES Vineyard(VineyardId)
);

--Insurance Policy, the percentage of the broker and the intermediary must either be null
--or they must be percentages
CREATE TABLE InsurancePolicy(
    PolicyId SERIAL,
    PolicyValidity DATERANGE NOT NULL 
        DEFAULT CAST('['||now() || ',' || now() + '1 year' || ']' AS DATERANGE),
    PackageId INTEGER NOT NULL,
    InsuredCapital MONEY NOT NULL,
    --these checks are not working
    PercentageBroker PercentageType,
    PercentageIntermediary PercentageType,
    ClientId VatOrFiscal,
    BrokerId RuiIdType,
    IntermediaryId RuiIdType,
    PRIMARY KEY(PolicyId),
    FOREIGN KEY(PackageId) REFERENCES InsurancePackage(PackageId),
    FOREIGN KEY(BrokerId) REFERENCES Broker(RuiId),
    FOREIGN KEY(IntermediaryId) REFERENCES Intermediary(RuiId),
    FOREIGN KEY(ClientId) REFERENCES Client(TaxCode),
    CONSTRAINT PercentageSumNotValid CHECK (
        PercentageIntermediary IS NULL 
        OR PercentageBroker IS NULL 
        OR PercentageBroker + PercentageIntermediary < 1
    )
);

--ProtectedBy
CREATE TABLE ProtectedBy(
    PolicyId INTEGER NOT NULL,
    VineyardId INTEGER NOT NULL,
    PRIMARY KEY(PolicyId, VineyardId),
    FOREIGN KEY(PolicyId) REFERENCES InsurancePolicy(PolicyId),
    FOREIGN KEY(VineyardId) REFERENCES Vineyard(VineyardId)
);
